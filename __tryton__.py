#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party Rollup',
    'name_de_DE': 'Partei Struktur',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
        Base module for party_rollup_* modules
        - This module installed solely doesn't provide any functionality.

        Install instead one or more of the modules:

        * party_rollup_organization
        * party_rollup_person
    ''',
    'description_de_DE': '''
        Basismodul der party_rollup_* Module
        - Die Installation nur dieses Moduls bietet keine Funktionalität

        Installieren Sie stattdessen eines oder mehrere der folgenden Module:

        * party_rollup_organization
        * party_rollup_person
    ''',
    'depends': [
        'company',
        'party_type',
    ],
    'xml': [
        'party.xml',
        'company.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
